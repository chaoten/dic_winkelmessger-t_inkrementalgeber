/******************************************************************************/
/*   (C) Copyright HTL - HOLLABRUNN  2009-2009 All rights reserved. AUSTRIA   */ 
/*                                                                            */ 
/* File Name:   External_Interrupt.c                                          */
/* Autor: 		Mathias Pichler, Andreas Sch�fberger                                            */
/* Version: 	V1.00                                                         */
/* Date: 		11/03/2012                                                    */
/* Description: Inkrementalgeber z�hlt rauf oder runter                         */
/******************************************************************************/
/* History: 	V1.00  creation										          */
/******************************************************************************/

#include <armv10_std.h>					


#define GPIOA_IDR GPIOA_BASE + 2*sizeof(uint32_t)
#define BITBAND_PERI(a,b) ((PERIPH_BB_BASE + (a-PERIPH_BASE)*32 + (b*4)))
#define Data *((volatile unsigned long *)(BITBAND_PERI(GPIOC_IDR,8)))			//Bitbanding f�r Inkrementalgeber auf Port PC8
#define Data_phase *((volatile unsigned long *)(BITBAND_PERI(GPIOC_IDR,9)))	//Bitbanding f�r Inkrementalgeber auf Port PC9
/*------------------------------ Function Prototypes -------------------------*/
static void NVIC_init(char position, char priority); 
static void EXTI1_Config(void);

/*------------------------------ Static Variables-------------------------*/
int falling_edges;

/******************************************************************************/
/*           External Interrupt Service Routine  Inkrementalgeber                    */
/******************************************************************************/
void EXTI1_IRQHandler(void)//ISR
{
	EXTI->PR |= (0x01 << 1); //Pending bit EXT0 r�cksetzen (Sonst wird die ISR immer wiederholt)
	falling_edges++;    // Fallende Flanke an PA1 erkannt
	return;
}  

/******************************************************************************/
/*                                   EXTI1_config                             */ 
/* Leitung PA1 wird mit EXTI1 verbunden, Interupt bei falling edge,Priorit�t 3*/ 
/******************************************************************************/
static void EXTI1_Config()	
{

	NVIC_init(EXTI1_IRQn,3);		//NVIC fuer initialisieren EXTI Line1 (Position 7, Priority 3) 

  RCC->APB2ENR |= 0x0001;		   //AFIOEN  - Clock enable	
	AFIO->EXTICR[0] &= 0xFFFFFF0F; //Interrupt-Line EXTI1 mit Portpin PA1 verbinden 
	EXTI->FTSR |= (0x01 << 1);	   //Falling Edge Trigger f�r EXIT1 Aktivieren
  EXTI->RTSR &=~(0x01 << 1);	   //Rising Edge Trigger f�r EXTI1 Deaktivieren

	EXTI->PR |= (0x01 << 1);	//EXTI_clear_pending: Das Ausl�sen auf vergangene Vorg�nge nach	dem enablen verhindern
	EXTI->IMR |= (0x01 << 1);   // Enable Interrupt EXTI1-Line. Kann durch den NVIC jedoch noch maskiert werden
}

/******************************************************************************/
/*                   NVIC_init(char position, char priority)    			  */ 
/* Funktion:                                                                  */    
/*   �bernimmt die vollst�ndige initialisierung eines Interrupts  im Nested   */
/*   vectored Interrupt controller (Priorit�t setzen, Ausl�sen verhindern,    */
/*   Interrupt enablen)                                                       */
/* �bergabeparameter: "position" = 0-67 (Nummer des Interrupts)               */
/*                    "priority": 0-15 (Priorit�t des Interrupts)		      */
/******************************************************************************/
static void NVIC_init(char position, char priority) 
{	
	NVIC->IP[position]=(priority<<4);	//Interrupt priority register: Setzen der Interrupt Priorit�t
	NVIC->ICPR[position >> 0x05] |= (0x01 << (position & 0x1F));//Interrupt Clear Pendig Register: Verhindert, dass der Interrupt ausl�st sobald er enabled wird 
	NVIC->ISER[position >> 0x05] |= (0x01 << (position & 0x1F));//Interrupt Set Enable Register: Enable interrupt
} 

/******************************************************************************/
/*                                MAIN function                               */
/******************************************************************************/
int main (void) 
{
int lcd_falling_edges;
char buffer[30];
void joy_inc_init(void);

	falling_edges=0;     // Zaehler f�r falling edges initialisieren
	lcd_falling_edges=0; 

    lcd_init ();	     // Initialisieren der LCD Anzeige
	lcd_clear();		 // LCD Anzeige l�schen
	sprintf(&buffer[0],"Fall. Edges=%d", lcd_falling_edges); // zaehler auf LCD aktualisieren
    lcd_put_string(&buffer[0]);


	do
  	{
			do {			 // falling edge
	   } while (Data ==1);
		falling_edges++;
		do {			 // falling edge
	  } while (Data ==0);
		if (lcd_falling_edges != falling_edges)    // Anzeigewert geaendert?
	 	{
			lcd_falling_edges = falling_edges;
			lcd_set_cursor(0,0);                 // Cursor auf Ursprung
			sprintf(&buffer[0],"Fall. Edges=%d", lcd_falling_edges); // zaehler auf LCD aktualisieren
    		lcd_put_string(&buffer[0]);
		}
  	} while (1);

}
